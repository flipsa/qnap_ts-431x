#!/bin/sh

# This script will set up a static route on a QNAP TS-431x
# WARNING: this script will overwrite autorun.sh !!!

# Attach the UBI mtd device
ubiattach -m 6 -d 2

# Mount the UBI mtd device
/bin/mount -t ubifs ubi2:config /tmp/config

# ll /tmp/config/

# Manually edit the autostart file
#vi /tmp/config/autorun.sh

# Use a here document to paste the relevant lines
cat << EOF > /tmp/config/autorun.sh
#!/bin/sh
ip route add 172.16.0.0/24 via 192.168.1.253 dev eth1 tab main
ip route add 172.16.0.0/24 via 192.168.1.253 dev eth1 tab 2
EOF

# Make it executable
chmod +x /tmp/config/autorun.sh

ls -la /tmp/config/autorun.sh

# Unmount device
umount /tmp/config

# Detach device
ubidetach -m 6


